require 'time'
require 'erb'
require 'logger'
require 'nokogiri'

class JiraRss2Html
  LOGGER = Logger.new(STDOUT, progname: 'JiraRss2Html')

  HTML_TEMPLATE = 'JiraRss2Html.html.erb'

  def convert(jirarss = nil)
    return unless jirarss

    xml = ::File.read(jirarss)
    doc = ::Nokogiri::XML(xml)
    doc.xpath('//item').children.each do |elem|
      instance_variable_set("@#{elem.name}".to_sym, elem)
    end
    unless @affect_versions
      @affect_versions = Nokogiri::XML::Node.new "affect_versions", doc
      @affect_versions.content = 'None'
    end
    unless @fix_versions
      @fix_versions = Nokogiri::XML::Node.new "fix_versions", doc
      @fix_versions.content = 'None'
    end
    if @labels.nil? || @labels.content.empty?
      @labels = Nokogiri::XML::Node.new "labels", doc
      @labels.content = 'None'
    end
    if @created
      @created.content = DateTime.parse(@created.content).strftime('%F %T')
    end
    if @updated
      @updated.content = DateTime.parse(@updated.content).strftime('%F %T')
    end
    if @resolved
      @resolved.content = DateTime.parse(@resolved.content).strftime('%F %T')
    end
    @comments.xpath('comment').each do |comment|
      LOGGER.debug(comment['created'])
      comment['created'] = DateTime.parse(comment['created']).strftime('%F %T')
    end
    begin
      template = ::File.read(HTML_TEMPLATE)
      puts ERB.new(template).result(binding)
    rescue => e
      LOGGER.warn("#{e.inspect} #{e.message}\n" + e.backtrace.first(25).join("\n  "))
    end
  end
end

if ::File.basename(__FILE__, '.rb') == 'jirarss2html'
  JiraRss2Html.new.convert(ARGV.shift)
end
